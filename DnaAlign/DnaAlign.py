from BLOSUM import makeBLOSUM
from WunschAlgorithm import WunschAlgorithm
import AlignAnalysisFnc as anl
gapPenalty = -100;
#gene 1
#query = "METTPLNSQKVLSECKDKEDCQENGVLQKGVPTPADKAEPGQISNGYSAVPSTSAGDEAPHSTPAATTTLVAEIHQGERETWGKKMDFLLSVIGYAVDLGNIWRFPYICYQNGGGAFLLPYTIMAIFGGIPLFYMELALGQYHRNGCISIWRKICPIFKGIGYAICIIAFYIASYYNTIIAWALYYLISSFTDQLPWTSCKNSWNTGNCTNYFAQDNITWTLHSTSPAEEFYLRHVLQIHQSKGLQDLGTISWQLALCIMLIFTIIYFSIWKGVKTSGKVVWVTATFPYIVLSVLLVRGATLPGAWRGVVFYLKPNWQKLLETGVWVDAAAQIFFSLGPGFGVLLAFASYNKFNNNCYQDALVTSVVNCMTSFVSGFVIFTVLGYMAEMRNEDVSEVAKDAGPSLLFITYAEAIANMPASTFFAIIFFLMLITLGLDSTFAGLEGVITAVLDEFPHIWAKRREWFVLIVVITCILGSLLTLTSGGAYVVTLLEEYATGPAVLTVALIEAVVVSWFYGITQFCSDVKEMLGFSPGWFWRICWVAISPLFLLFIICSFLMSPPQLRLFQYNYPHWSIILGYCIGTSSVICIPIYIIYRLISTPGTLKERIIKSITPETPTEIPCGDIRMNAV"
#subject = "METTPLNSQKELSVCKDGEDCQENGLLRKGVPASGDKVESGQISNGYSAVPSPGAGDDTPHSIPAATTALVAEVHSGERETWGKKMDFLLSVIGYAVDLGNVWRFPYICYQNGGGAFLLPYTIMAIFGGIPLFYMELALGQYHRNGCISIWRKICPIFKGIGYAICIIAFYIASYYNTIMAWALYYLISSFTDQLPWTSCKNSWNTGNCTNYFSEDNITWTLHSTSPAEEFYTRHVLQIHRSKGLQDLGGVSWQLALCIMLIFTIIYFSIWPGPCLPGPSSRRHVLQIHRSKGLQDLGGVSWQLALCIMLIFTIIYFSIWKGVKTSGKVVWVTATFPYIILLVLLVRGATLPGAWRGVLFYLKPNWQKLLETGVWVDAAAQIFFSLGPGFGVLLAFASYNKFNNNCYQDALVTSLVNCMTSFVSGFVIFTVLGYMAEMRKEDVSEVAKDAGPSLLFITYAEAIANMPASTFFAIIFFLMLITLGLDSTGWALKDCKQEKFAGLEGVITAVLDEFPHIWSKRRELLVLGVVVTCFFGSLFTLTFGGAYVVKLLEEYATGPAVLTVALIEAMAVFWFYGITQFCSDVKEMLGFSPGWFWRICWVAISPLFLLFIICSFLMSPPQLRLFQYTYPHWSVVLGYCIGTSSFICIPTYIIYRLIITPGTLKERIIKSITPETPTEIPCGDIRLNAV"

#gene2
query = "MTELPAPLSYFQNAQMSEDNHLSNTNDNRERQEHNDRRSLGHPEPLSNGRPQGNSRQVVEQDEEEDEELTLKYGAKHVIMLFVPVTLCMVVVVATIKSVSFYTRKDGQLIYTPFTEDTETVGQRALHSILNAAIMISVIVVMTILLVVLYKYRCYKVIHAWLIISSLLLLFFFSFIYLGEVFKTYNVAVDYITVALLIWNFGVVGMISIHWKGPLRLQQAYLIMISALMALVFIKYLPEWTAWLILAVISVYDLVAVLCPKGPLRMLVETAQERNETLFPALIYSSTMVWLVNMAEGDPEAQRRVSKNSKYNAESTERESQDTVAENDDGGFSEEWEAQRDSHLGPHRSTPESRAAVQELSSSILAGEDPEERGVKLGLGDFIFYSVLVGKASATASGDWNTTIACFVAILIGLCLTLLLLAIFKKALPALPISITFGLVFYFATDYLVQPFMDQLAFHQFYI"
subject = "MTELPAPLSYFQNAQMSEDNHSSTAIRNQQEQRQQQRRRRSNPESVSNGRPQGGGQQALEQEEEDDEELTLKYGAKHVIMLFVPVTLCMVVVVATIKSVSFYTRKDGQLIYTPFTEDTETVGQRALNSILNAIIMISVIVVMTILLVVLYKYRCYKVIHGWLIISSLLLLFFFSFIYLGEVFKTYNVAMDYITVALLIFNFGVVGMIAIHWKGPLRLQQAYLIMISALMALVFIKYLPEWTAWLILAVISVYDLVAVLCPKGPLRMLVETAQERNETLFPALIYSSTMVWLVNMAEGDPEAQRKVSKTSNHNAQSSEGNSQDPVTESDDGGFSEEWEAQRDSRLGPHRSTAETRAAAQEISSSILTSEDPEERGVKLGLGDFIFYSVLVGKASATASGDWNTTIACFVAILIGLCLTLLLLAIFKKALPALPISITFGLVFYFATDYLVQPFMDQLAFHQFYI"
wunsch = WunschAlgorithm()
wunsch.initialize(query,subject,makeBLOSUM(),gapPenalty)
wunsch.execute()
print "exact matches:",anl.countMatches(wunsch.adjustedQuery,wunsch.adjustedSubject)
print "longest consecutive matches:",anl.longest_consecutive(wunsch.adjustedQuery,wunsch.adjustedSubject)