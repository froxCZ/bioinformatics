class WunschAlgorithm:
    negativePolar = ['D','E']
    positivePolar = ['R','K','H']
    unchargedPolar = ['N','Q','S','T','Y']
    nonPolar = ['A', 'G', 'V', 'L', 'I', 'P', 'F', 'M', 'W', 'C']
    aminoAcidGroups = [negativePolar,positivePolar,unchargedPolar,nonPolar]
    initialized = False
    adjustedQuery = ""
    adjustedSubject = ""
    def initialize(self,q,s,matrix,gapPenalty):
        self.setStrings(q,s)
        self.setMatrix(matrix)
        self.setGapPenalty(gapPenalty)
        self.initialized = True
    def setStrings(self,strA,strB):
        self.query = strA
        self.subject = strB
    def setMatrix(self,matrix):
        self.similarityMatrix = matrix
    def setGapPenalty(self,gapPenalty):
        self.gapPenalty = gapPenalty
    def printStrings(self):
        print self.query," B:",self.subject    
    def createMatrix(self):
        matrix = []
        #query is column and subject is row
        for c in range(len(self.query)+1):
            matrix.append([None]*(len(self.subject)+1))
        return matrix
    def createScoreMatrix(self):
        scoreMatrix = self.createMatrix()
        cumulatedPenalty = 0
        for list in scoreMatrix:
            list[0] = cumulatedPenalty
            cumulatedPenalty+=self.gapPenalty
        cumulatedPenalty = 0
        scoreMatrix[0] = [i*self.gapPenalty for i in range(len(scoreMatrix[0]))]
        return scoreMatrix
    def createRouteMatrix(self):
        routeMatrix = self.createMatrix()
        for list in routeMatrix:
            list[0] = 'T'
        routeMatrix[0] = ['L' * (len(routeMatrix[0]))]
        return routeMatrix
    def similarAcids(self,a,b):
        for group in self.aminoAcidGroups:
            if(group.count(a) >0 and group.count(b)>0):
                return True
        return False
    def fillMatrixes(self):
        self.scoreMatrix = self.createScoreMatrix()
        self.routeMatrix = self.createRouteMatrix()        
        for row in range(1,len(self.query)+1):
            for col in range(1,len(self.subject)+1):
                fromDiagonal = self.scoreMatrix[row-1][col-1]+self.similarityMatrix[self.query[row-1]][self.subject[col-1]]
                fromLeft = self.scoreMatrix[row][col-1]+self.gapPenalty
                fromTop = self.scoreMatrix[row-1][col]+self.gapPenalty                
                bestMove = (fromDiagonal,'D')
                if(fromLeft > bestMove[0]):
                    bestMove = (fromLeft,'L')
                if(fromTop > bestMove[0]):
                    bestMove = (fromTop,'T')
                self.scoreMatrix[row][col],self.routeMatrix[row][col] = bestMove  
    def createBestRoute(self):
        self.route = []
        row = len(self.query)
        col = len(self.subject)
        while(row != 0 or col != 0):
            move = self.routeMatrix[row][col]
            if(move=='D'):
                row-=1
                col-=1
            elif(move == 'L'):
                col-=1
            elif(move=='T'):
                row-=1
            self.route.insert(0,move)                

    def adjustStrings(self):
        self.adjustedQuery = ""
        self.adjustedSubject = ""
        queryPtr = 0
        subjectPtr = 0
        #T - top means dash in subject and vice versa
        for move in self.route:
            if(move == 'D'):
                self.adjustedQuery+=self.query[queryPtr]                    
                self.adjustedSubject+=self.subject[subjectPtr]
                queryPtr+=1
                subjectPtr+=1
            elif(move == 'T'):
                self.adjustedQuery+=self.query[queryPtr]
                self.adjustedSubject+="-"
                queryPtr+=1
            elif(move == 'L'):
                self.adjustedQuery+="-"
                self.adjustedSubject+=self.subject[subjectPtr]
                subjectPtr+=1
    def createAligment(self):
        self.aligment = ""
        for i in range(0,len(self.adjustedQuery)):
            if(self.adjustedQuery[i] == self.adjustedSubject[i]):
                self.aligment+=self.adjustedQuery[i]
            elif(self.similarAcids(self.adjustedQuery[i],self.adjustedSubject[i])):
                self.aligment+="+"
            else:
                self.aligment+=" "

    def printResults(self):   
        queryChunks = [self.adjustedQuery[100*i:100*(i+1)] for i in range(len(self.adjustedQuery)/100)]     
        alignmentChunks = [self.aligment[100*i:100*(i+1)] for i in range(len(self.aligment)/100)]     
        subjectChunks = [self.adjustedSubject[100*i:100*(i+1)] for i in range(len(self.adjustedSubject)/100)]     
        #print formatted result by 100 chars per line
        for i in range(0,len(queryChunks)):
            print "Query\t", queryChunks[i]
            print i*100,"\t", alignmentChunks[i]
            print "Sbjct\t", subjectChunks[i]
            print
        #print "Query\t",self.adjustedQuery
        #print "\t",self.aligment
        #print "Sbjct\t",self.adjustedSubject
    def execute(self): 
        if(self.initialized == False):
            print "Call wunsch.initialize() to set necessary input values"
            return
        self.fillMatrixes() 
        self.createBestRoute() 
        self.adjustStrings()        
        self.createAligment()
        self.printResults()        
                
       
                                                           
